AntiFeatures:UpstreamNonFree
Categories:Navigation
License:Apache2
Web Site:http://www.cgeo.org
Source Code:https://github.com/cgeo/c-geo-opensource
Issue Tracker:https://github.com/cgeo/c-geo-opensource/issues
Donate:https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=cgeo%40cgeo%2eorg&item_name=Donate%20to%20CGeo%20via%20F-droid

Auto Name:c:geo
Summary:Geocaching client
Description:
Geocaching is a sport where people hide things and you go and find them. This
is an unnofficial client to access geocaching.com's list of caches, search and
save them to the device.

This build does not support proprietary Google Maps, but OSM maps (Mapnik and
Cyclemap) are available.

You will need a geocaching.com account to the unlock the exact location of the
caches, and a premium membership to sort caches by distance.

Read Recent Changes, FAQ, and About in c:geo, before submitting issues!

Updates are unlikely, as it needs proprietary SDKs to build, as well as multiple
'secret' keys.
.

Repo Type:git
Repo:https://github.com/cgeo/c-geo-opensource.git

#Uses version name from Google Play, with '-fdroid' suffix, Auto-updated - tag
#is always market_nnnnn where nnnnn is the version code.
#Dummy API keys are used, but future versions should just disable the Google
#Maps functionality.
#Force target as API 11 isn't on build server
Build:2012.06.18,20120618
    commit=388a5a3b1c0885a
    subdir=main
    target=Google Inc.:Google APIs:15
    forceversion=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml

Build:2012.08.15,20120815
    disable=No corresponding source
    commit=unknown - see disabled

Build:2012.12.03,20121203
    commit=market_20121203
    subdir=main
    target=Google Inc.:Google APIs:16
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml

Build:2013.01.10-fdroid,20130110
    commit=market_20130110
    subdir=main
    target=Google Inc.:Google APIs:16
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml

Build:2013.02.07-fdroid,20130207
    commit=market_20130207
    subdir=main
    target=Google Inc.:Google APIs:16
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml

Build:2013.02.21-fdroid,20130221
    commit=market_20130221
    subdir=main
    target=Google Inc.:Google APIs:16
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml

Build:2013.04.03-fdroid,20130403
    commit=market_20130403
    subdir=main
    target=Google Inc.:Google APIs:16
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml

Build:2013.05.13-fdroid,20130513
    commit=market_20130513
    subdir=main
    target=Google Inc.:Google APIs:16
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml

Build:2013.05.16-fdroid,20130516
    commit=market_20130516
    subdir=main
    target=Google Inc.:Google APIs:16
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml

Build:2013.05.18-fdroid,20130518
    disable=No such tag market_20130518
    commit=unknown - see disabled
    subdir=main
    target=Google Inc.:Google APIs:16
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml

Build:2013.05.22-fdroid,20130522
    commit=market_20130522
    subdir=main
    target=Google Inc.:Google APIs:16
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml

Build:2013.05.31a-fdroid,20130532
    commit=market_20130532
    subdir=main
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml

Build:2013.06.05-fdroid,20130605
    commit=market_20130605
    subdir=main
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml

Build:2013.07.25-fdroid,20130725
    commit=market_20130725
    subdir=main
    forceversion=yes
    forcevercode=yes
    prebuild=cp templates/private.properties . && \
        cp templates/mapsapikey.xml res/values/ && \
        sed -i 's@\(maps.api.key=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's@\(maps.api.key.market=\).*@\1LhWN5eWaqRrP0ZgO3h7vevsAwtJJs80C-G9o7RA@' private.properties && \
        sed -i 's/\@maps.api.key\@/${maps.api.key}/g' res/values/mapsapikey.xml

Auto Update Mode:None
Update Check Mode:None
Current Version:2013.07.25-fdroid
Current Version Code:20130725

