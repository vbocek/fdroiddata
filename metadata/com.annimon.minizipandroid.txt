Categories:Office
License:Zlib
Web Site:https://github.com/aNNiMON/MinizipAndroid
Source Code:https://github.com/aNNiMON/MinizipAndroid
Issue Tracker:https://github.com/aNNiMON/MinizipAndroid/issues

Auto Name:Minizip Android
Summary:Archive manager
Description:
Manage, zip and unzip archives.
.

Repo Type:git
Repo:https://github.com/aNNiMON/MinizipAndroid.git

Build:0.1,1
    disable=doesn't seem to work with regular zip files, also happens with upstream's build
    commit=7d98ab8665f6f8f16c618a1ad1e7cf39c1d04f12
    buildjni=yes
    target=android-17
    scandelete=libs/armeabi/libminizip.so

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.1
Current Version Code:1
