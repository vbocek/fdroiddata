Categories:Games
License:GPLv3
Web Site:http://gbcoid.sf.net
Source Code:http://sf.net/p/gbcoid/code
Issue Tracker:http://sf.net/p/gbcoid/tickets

Auto Name:GBCoid
Summary:Gameboy Color (Nintendo) emulator
Description:
This project is based on sources made available by original GBCoid developer,
who in turn used code of gnuboy.
.

Repo Type:git
Repo:git://git.code.sf.net/p/gbcoid/code

Build:1.8.5,32
    commit=1.8.5
    subdir=GBCoid
    submodules=yes
    target=android-15
    patch=target_api_10.diff
    buildjni=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.8.5
Current Version Code:32

