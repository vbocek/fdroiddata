Categories:Science & Education
License:Apache2
Web Site:https://code.google.com/p/android-heart-rate-monitor
Source Code:https://code.google.com/p/android-heart-rate-monitor/source
Issue Tracker:https://code.google.com/p/android-heart-rate-monitor/issues

Auto Name:HeartRateMonitor
Summary:Heart Rate Monitor
Description:
A simple heart rate monitor, using the device's camera to attempt to detect
a pulse in your finger.
.

Repo Type:git-svn
Repo:https://android-heart-rate-monitor.googlecode.com/svn/trunk

Build:1.0,1
    commit=10
    target=android-7

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0
Current Version Code:1

