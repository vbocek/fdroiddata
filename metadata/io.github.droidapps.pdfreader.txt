Categories:Office,Reading
License:GPLv3
Web Site:https://droidapps.github.io/pdfreader4Android/
Source Code:https://github.com/droidapps/pdfreader4Android
Issue Tracker:https://github.com/droidapps/pdfreader4Android/issues

Name:PDF Reader
Auto Name:PDF Reader
Summary:View PDF files
Description:
Simple PDF reader based on [[cx.hell.android.pdfview]].

Features:
* Textual search support
* Flexible key/button/gesture configuration
* Different color schemes for comfortable reading
* Optimizations for eInk
* x86 support
* Pinch to zoom
* Bookmarks
.

Repo Type:git
Repo:https://github.com/droidapps/pdfreader4Android

Build:0.4.0,40000
    disable=binaries, unmaintainable, patches fail
    commit=ea5730b4108ac76352b3dcfc5e8cffefad9a6008
    extlibs=android/android-support-v4.jar
    prebuild=sed -i '/proguard/d' project.properties && \
        cd deps && \
        tar xvf freetype-2.4.10.tar.bz2 && \
        tar xvf jpegsrc.v8d.tar.gz && \
        tar xvf mupdf-a3d00b2c51c1df23258f774f58268be794384c27.tar.bz2 && \
        tar xvf openjpeg-1.5.1.tar.gz && \
        tar xvf jbig2dec-0.11.tar.gz && \
        cp openjpeg-1.5.1/libopenjpeg/*.[ch] ../jni/openjpeg/ && \
        echo '#define PACKAGE_VERSION' '"'openjpeg-1.5.1'"' > ../jni/openjpeg/opj_config.h && \
        cp jpeg-8d/*.[ch] ../jni/jpeg/ && \
        cp jbig2dec-0.11/* ../jni/jbig2dec/ && \
        for x in draw fitz pdf ; do cp -r mupdf-a3d00b2c51c1df23258f774f58268be794384c27/$x/*.[ch] ../jni/mupdf/$x/ ; done && \
        cd .. && \
        patch jni/mupdf/fitz/fitz.h jni/mupdf-apv/fitz/apv_fitz.h.patch && \
        patch -o jni/mupdf-apv/fitz/apv_doc_document.c jni/mupdf/fitz/doc_document.c jni/mupdf-apv/fitz/apv_doc_document.c.patch && \
        patch -o jni/mupdf-apv/pdf/apv_pdf_cmap_table.c jni/mupdf/pdf/pdf_cmap_table.c jni/mupdf-apv/pdf/apv_pdf_cmap_table.c.patch && \
        patch -o jni/mupdf-apv/pdf/apv_pdf_fontfile.c jni/mupdf/pdf/pdf_fontfile.c jni/mupdf-apv/pdf/apv_pdf_fontfile.c.patch && \
        cd - && \
        cp -r freetype-2.4.10/{src,include} ../jni/freetype/ && \
    target=android-17
    buildjni=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:0.4.0
Current Version Code:40000

