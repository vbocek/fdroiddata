Categories:Internet
License:GPLv3+
Web Site:http://openkeychain.org
Source Code:https://github.com/open-keychain/open-keychain
Issue Tracker:https://github.com/open-keychain/open-keychain/issues

Auto Name:OpenKeychain
Summary:Manage OpenPGP keys
Description:
OpenKeychain allows you to manage cryptographic keys and encrypt messages
as well as files for your contacts.

It implements the OpenPGP standard, also often referred to as GPG or PGP.

Features:
* All basic OpenPGP operations: encrypt/decrypt/sign/verify
* NFC/QR code key exchange
* Key management (import/export/sign/upload)
* Import/export of GPG key rings and exported keys from/to the SD card
* Keyserver support
* Easy API

It doesn't integrate with any email client yet, but that is a planned feature.
.

Repo Type:git
Repo:https://github.com/open-keychain/open-keychain.git

Build:2.1,21000
    commit=v2.1
    subdir=OpenPGP-Keychain
    update=.,../libraries/ActionBarSherlock,../libraries/HtmlTextView

Build:2.1.1,21100
    commit=v2.1.1
    subdir=OpenPGP-Keychain
    update=.,../libraries/ActionBarSherlock,../libraries/HtmlTextView

Build:2.2,22000
    commit=v2.2
    subdir=OpenPGP-Keychain
    gradle=yes

Build:2.3,23000
    commit=v2.3
    subdir=OpenPGP-Keychain
    gradle=yes

Build:2.3.1,23100
    commit=v2.3.1
    subdir=OpenPGP-Keychain
    gradle=yes

Build:2.4,24000
    commit=v2.4
    subdir=OpenPGP-Keychain
    gradle=yes

Build:2.5,25000
    commit=v2.5
    subdir=OpenPGP-Keychain
    gradle=yes

Build:2.6,26000
    commit=v2.6
    subdir=OpenKeychain
    submodules=yes
    gradle=yes

Build:2.6.1,26100
    commit=v2.6.1
    subdir=OpenKeychain
    submodules=yes
    gradle=yes

Build:2.7,27000
    commit=v2.7
    subdir=OpenKeychain
    submodules=yes
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:2.7
Current Version Code:27000

